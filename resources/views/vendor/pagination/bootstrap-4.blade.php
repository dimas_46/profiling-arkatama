<div class="text-start py-4">
    <div class="custom-pagination">
        @if ($paginator->hasPages())
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <a href="#" class="prev disabled">Prevous</a>
            @else
                <a href="{{ $paginator->previousPageUrl() }}" class="prev" rel="prev">Prevous</a>
            @endif

            {{-- Page Links --}}
            @foreach ($elements as $element)
                @if (is_string($element))
                    <a href="#" class="active">{{ $element }}</a>
                @endif

                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <a href="#" class="active">{{ $page }}</a>
                        @else
                            <a href="{{ $url }}">{{ $page }}</a>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <a href="{{ $paginator->nextPageUrl() }}" class="next" rel="next">Next</a>
            @else
                <a href="#" class="next disabled">Next</a>
            @endif
        @endif
    </div>
</div>

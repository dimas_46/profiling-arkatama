<ul class="menu-inner py-1">
    <!-- Dashboard -->
    <li class="menu-item {{ request()->is('dashboard') ? 'active' : '' }}">
        <a href="{{ route('admin-dashboard') }}" class="menu-link">
            <i class="menu-icon tf-icons bx bx-home-circle"></i>
            <div data-i18n="Analytics">Dashboard</div>
        </a>
    </li>
    <li class="menu-item {{ request()->is('blog*') ? 'active' : '' }}">
        <a href="{{ route('admin-blog-manage') }}" class="menu-link">
            <i class='menu-icon bx bxl-blogger'></i>
            <div data-i18n="Analytics">Post</div>
        </a>
    </li>
    <li class="menu-item {{ request()->is('kategori*') ? 'active' : '' }}">
        <a href="{{ route('admin-kategori-manage') }}" class="menu-link">
            <i class='menu-icon bx bxl-kickstarter'></i>
            <div data-i18n="Analytics">Kategori</div>
        </a>
    </li>

    {{-- <li class="menu-header small text-uppercase">
        <span class="menu-header-text">Jemaat</span>
    </li>
    <li
        class="{{ Str::startsWith(url()->current(), url('jemaat*')) || request()->is('jemaat*') ? 'menu-item active open' : 'menu-item' }}">
        <a href="#" class="menu-link menu-toggle ">
            <i class="menu-icon tf-icons bx bx-group"></i>
            <div data-i18n="Account Settings">Users</div>
        </a>
        <ul class="menu-sub">
            <li class="menu-item {{ request()->is('jemaat*') ? 'active' : '' }}">
                <a href="#" class="menu-link ">
                    <div data-i18n="Account">Data Jemaat</div>
                </a>
            </li>
        </ul>
    </li> --}}



</ul>

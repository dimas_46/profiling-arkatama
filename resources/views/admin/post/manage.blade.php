@extends('admin.layout.default')
@section('title', 'Manage Blog')

@section('content')
    <div class="col-md-12">
        <div id="flash" data-flash="{{ session()->get('success') }}"></div>
        @if ($errors->any())
            <ul>
                @foreach ($errors->all() as $error)
                    <div id="flashError" data-flash="{{ $error }}">
                    </div>
                @endforeach
            </ul>
        @endif
        <div class="card">
            <div class="card-header">
                <a style="float:right" href="#" class="btn btn-outline-success" data-bs-toggle="modal"
                    data-bs-target="#addModal"><i class="bx bx-plus"></i> &nbspTambah</a>
            </div>
            <div class="card-body">
                <table id="postTable" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Thumbnail</th>
                            <th>Judul</th>
                            <th>Kategori</th>
                            <th>Tanggal</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($post as $row)
                            <div class="modal fade" id="editModal{{ $row->id }}" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="modalCenterTitle">Edit Posting</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{ route('admin-blog-update', ['id' => $row->id]) }}"
                                                method="post" enctype="multipart/form-data">
                                                @csrf
                                                @method('PUT')
                                                <div class="col mb-3">
                                                    <label for="nameWithTitle" class="form-label">Judul</label><span
                                                        class="text-danger">*</span>
                                                    <input type="text" name="judul" id="nameWithTitle"
                                                        class="form-control @error('judul') is-invalid @enderror"
                                                        value="{{ $row->judul }}" placeholder="Nama Judul" />
                                                    @error('judul')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col mb-3">
                                                    <label for="thumbnail" class="form-label">Thumbnail</label><span
                                                        class="text-danger">*</span>
                                                    <input type="file" name="thumbnail" id="thumbnailInput"
                                                        class="form-control @error('thumbnail') is-invalid @enderror">
                                                    <span class="text-danger" style="font-size: 14px">Max : 2Mb</span>
                                                    <div class="mt-2">
                                                        <center>
                                                            <img id="thumbnailPreview"
                                                                src="{{ asset('image_blog/thumbnail/' . $row->thumbnail) }}"
                                                                style="max-width: 100%; max-height: 200px;">
                                                        </center>
                                                    </div>
                                                    @error('thumbnail')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col mb-3">
                                                    <label for="nameWithTitle" class="form-label">Image</label>
                                                    <input type="file" name="image[]" id="nameWithTitle" multiple
                                                        class="form-control @error('image') is-invalid @enderror" />
                                                    <span>Optional</span> <span style="float:right">Saran Ukuran :
                                                        1270x720px</span>
                                                    @error('image')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col mb-3">
                                                    <label for="nameWithTitle" class="form-label">Deskripsi</label><span
                                                        class="text-danger">*</span>
                                                    <textarea name="deskripsi" class="form-control @error('deskripsi') is-invalid @enderror" cols="30" rows="4">{{ $row->deskripsi }}</textarea>
                                                    @error('deskripsi')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                                <div class="col mb-3">
                                                    <label for="nameWithTitle" class="form-label">Kategori</label>
                                                    <select name="kategori_id"
                                                        class="form-control @error('kategori_id') is-invalid @enderror">
                                                        @foreach ($kategori as $item)
                                                            <option value="{{ $item->id }}"
                                                                {{ $row->kategori_id == $item->id ? 'selected' : '' }}>
                                                                {{ $item->nama_kategori }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    @error('kategori_id')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-secondary"
                                                data-bs-dismiss="modal">
                                                Close
                                            </button>
                                            <button type="submit" class="btn btn-primary"> Save </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    <center>
                                        <img src="{{ asset('image_blog/thumbnail/' . $row->thumbnail) }}" height="200"
                                            alt="">
                                    </center>
                                </td>
                                <td>{{ $row->judul }}</td>
                                <td>{{ $row->kategori->nama_kategori }}</td>
                                <td>{{ \Carbon\Carbon::parse($row->tanggal)->format('d F Y') }}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary dropdown-toggle"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            Action
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="#" data-bs-toggle="modal"
                                                    data-bs-target="#editModal{{ $row->id }}"><i
                                                        class="bx bx-pencil"></i> Edit</a></li>
                                            <li>
                                                <a href="javascript::void(0)" class="dropdown-item"
                                                    onclick="confirmDelete('#delete-{{ $row->id }}')"><i
                                                        class="bx bx-trash"></i>&nbsp
                                                    Delete</a>
                                            </li>
                                            <li><a class="dropdown-item"
                                                    href="{{ route('admin-blog-details', ['id' => $row->id]) }}"><i
                                                        class="bx bx-detail"></i>&nbsp
                                                    Details</a></li>
                                            <li>
                                        </ul>
                                    </div>
                                </td>
                                <form method="post" action="{{ route('admin-blog-delete', ['id' => $row->id]) }}"
                                    id="delete-{{ $row->id }}">
                                    @csrf
                                    @method('delete')
                                </form>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"
        integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
    <link href="https://cdn.datatables.net/v/bs5/dt-1.13.4/datatables.min.css" rel="stylesheet" />
    <script src="https://cdn.datatables.net/v/bs5/dt-1.13.4/datatables.min.js"></script>
    <script>
        let table = $('#postTable').DataTable({
            searching: true,
            pagination: true
        });
        $('#postTable_filter input[type="search"]').attr('placeholder', 'Search All Fields').addClass(
            'form-control');
        $('#searchBox').on('keyup', function() {
            table.search(this.value).draw();
        });
    </script>
    <div class="modal fade" id="addModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalCenterTitle">Tambah Posting</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin-blog-store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="col mb-3">
                            <label for="nameWithTitle" class="form-label">Judul</label><span class="text-danger">*</span>
                            <input type="text" name="judul" id="nameWithTitle"
                                class="form-control @error('judul') is-invalid @enderror" value="{{ old('judul') }}"
                                placeholder="Nama Judul" />
                            @error('judul')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col mb-3">
                            <label for="thumbnail" class="form-label">Thumbnail</label><span class="text-danger">*</span>
                            <input type="file" name="thumbnail" id="thumbnailInput"
                                class="form-control @error('thumbnail') is-invalid @enderror">
                            <div class="mt-2">
                                <center>
                                    <img id="thumbnailPreview" src="#"
                                        style="max-width: 100%; max-height: 200px;">
                                </center>
                            </div>
                            @error('thumbnail')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col mb-3">
                            <label for="nameWithTitle" class="form-label">Image</label>
                            <input type="file" name="image[]" id="nameWithTitle" multiple
                                class="form-control @error('image') is-invalid @enderror" />
                            <span>Optional</span> <span class="float-right">Ukuran : </span>
                            @error('image')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col mb-3">
                            <label for="nameWithTitle" class="form-label">Deskripsi</label><span
                                class="text-danger">*</span>
                            <textarea name="deskripsi" class="form-control @error('deskripsi') is-invalid @enderror" cols="30"
                                rows="4">{{ old('deskripsi') }}</textarea>
                            @error('deskripsi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="col mb-3">
                            <label for="nameWithTitle" class="form-label">Kategori</label>
                            <select name="kategori_id" class="form-control @error('kategori_id') is-invalid @enderror">
                                <option value="">-- Select Kategori --</option>
                                @foreach ($kategori as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama_kategori }}</option>
                                @endforeach
                            </select>
                            @error('kategori_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary"> Save </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        const thumbnailInput = document.getElementById('thumbnailInput');
        const thumbnailPreview = document.getElementById('thumbnailPreview');

        thumbnailInput.addEventListener('change', function() {
            const file = thumbnailInput.files[0];

            if (file) {
                thumbnailPreview.style.display = 'block';
                const reader = new FileReader();

                reader.addEventListener('load', function(event) {
                    thumbnailPreview.src = event.target.result;
                });

                reader.readAsDataURL(file);
            } else {
                thumbnailPreview.style.display = 'none';
            }
        });
    </script>
@endSection

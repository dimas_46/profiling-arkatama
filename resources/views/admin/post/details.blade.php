@extends('admin.layout.default')
@section('title', 'Detail Blog')

@section('content')
    <div class="card">
        <div class="card-header">
            <a href="{{ route('admin-blog-manage') }}" class="btn btn-warning" style="float:right"> <i
                    class="bx bx-arrow-back"></i>&nbsp Kembali</a>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                        <div class="carousel-inner">
                            @foreach ($post->images as $key => $image)
                                <div class="carousel-item {{ $key == 0 ? 'active' : '' }}">
                                    <img class="img-fluid d-block w-100"
                                        src="{{ asset('image_blog/image/' . $image->image) }}"
                                        alt="Slide {{ $key + 1 }}">
                                </div>
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button"
                            data-bs-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only"></span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button"
                            data-bs-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <img class="card-img-top" src="{{ asset('image_blog/thumbnail/' . $post->thumbnail) }}">
                <div class="card-body">
                    <div class="card-title">
                        {{-- <span style="float:right">
                            Published :
                        </span> --}}
                        <h4 class="mb-1">{{ $post->judul }}</h4>

                        <span class="mt-0">{{ $post->kategori->nama_kategori }} •
                            {{ \Carbon\Carbon::parse($post->tanggal)->format('d F Y') }}</span>
                    </div>
                    <p>
                        {{ $post->deskripsi }}
                    </p>

                </div>
            </div>
        </div>
    </div>


@endSection

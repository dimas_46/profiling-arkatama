@extends('admin.layout.default')
@section('title', 'Dashboard')

@section('content')
    <script src="https://cdn.amcharts.com/lib/4/core.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
    <script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
    <div class="row mt-3">
        <div class="col-md-6 mt-3">
            <div class="card">
                <div class="card-header">
                    Jumlah Berita
                </div>
                <div class="card-body">
                    <h5 class="card-title">{{ $post }}</h5>
                </div>
            </div>
        </div>

        <div class="col-md-6 mt-3">
            <div class="card">
                <div class="card-header">
                    Jumlah Kategori
                </div>
                <div class="card-body">
                    <h5 class="card-title">{{ $kategori }}</h5>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mt-3">
        <div class="card">
            <div class="card-header">

            </div>
            <div class="card-body">
                <div id="chartDiv" style="width: 100%; height: 400px;"></div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.7.0.min.js"
        integrity="sha256-2Pmvv0kuTBOenSvLm6bvfBSSHrUJ+3A7x6P5Ebd07/g=" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function() {
            // Fetch data using Ajax
            $.ajax({
                url: "{{ route('admin-dashboard-chart-data') }}",
                type: "GET",
                dataType: "json",
                success: function(data) {
                    // Create the chart
                    am4core.ready(function() {
                        am4core.useTheme(am4themes_animated);
                        var chart = am4core.create("chartDiv", am4charts.XYChart);

                        // Add data to the chart
                        chart.data = data;

                        // Create axes
                        var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
                        categoryAxis.dataFields.category = "nama_kategori";
                        categoryAxis.renderer.grid.template.location = 0;

                        var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());

                        // Create series
                        var series = chart.series.push(new am4charts.ColumnSeries());
                        series.dataFields.valueY = "post_count";
                        series.dataFields.categoryX = "nama_kategori";
                        series.name = "Posts";
                        series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
                    });
                }
            });
        });
    </script>

@endSection()

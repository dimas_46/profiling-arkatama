@extends('admin.layout.default')
@section('title', 'Kategori')

@section('content')


    <div class="col-md-12">
        <div id="flash" data-flash="{{ session()->get('success') }}"></div>
        @if ($errors->any())
            <ul>
                @foreach ($errors->all() as $error)
                    <div id="flashError" data-flash="{{ $error }}">
                    </div>
                @endforeach
            </ul>
        @endif
        <div class="card">
            <div class="card-header">
                <a style="float:right" href="#" class="btn btn-outline-success" data-bs-toggle="modal"
                    data-bs-target="#addModal"><i class="bx bx-plus"></i> &nbspTambah</a>
            </div>
            <div class="card-body">

                <table id="kategoriTable" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Kategori</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($kategori as $row)
                            <div class="modal fade" id="editModal{{ $row->id }}" tabindex="-1" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="modalCenterTitle">Edit Kategori Blog</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{ route('admin-kategori-update', ['id' => $row->id]) }}"
                                                method="post">
                                                @csrf
                                                @method('PUT')
                                                <div class="col mb-3">
                                                    <label for="nameWithTitle" class="form-label">Nama Kategori</label><span
                                                        class="text-danger">*</span>
                                                    <input type="text" name="nama_kategori" id="nameWithTitle"
                                                        class="form-control @error('nama_kategori') is-invalid @enderror"
                                                        value="{{ $row->nama_kategori }}" placeholder="Nama Kategori" />
                                                    @error('nama_kategori')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-outline-secondary"
                                                data-bs-dismiss="modal">
                                                Close
                                            </button>
                                            <button type="submit" class="btn btn-primary"> Save </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $row->nama_kategori }}</td>
                                <td>
                                    @if ($row->status == 1)
                                        <span class="badge bg-label-success me-1">Active</span>
                                    @else
                                        <span class="badge bg-label-danger me-1r">Not Active</span>
                                    @endif
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary dropdown-toggle"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            Action
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a class="dropdown-item" href="#" data-bs-toggle="modal"
                                                    data-bs-target="#editModal{{ $row->id }}"><i
                                                        class="bx bx-pencil"></i> Edit</a></li>
                                            <li>
                                                <a href="javascript::void(0)" class="dropdown-item"
                                                    onclick="confirmDelete('#delete-{{ $row->id }}')"><i
                                                        class="bx bx-trash"></i>&nbsp
                                                    Delete</a>
                                            </li>
                                            <li>
                                                <form class="d-inline"
                                                    action="{{ route('admin-kategori-updateStatus', ['id' => $row->id]) }}"
                                                    method="post">
                                                    @csrf
                                                    @method('PUT')
                                                    @if ($row->status == '0')
                                                        <button type="submit" name="status" value="1"
                                                            class="dropdown-item">
                                                            <i class="bx bx-power-off"></i>&nbsp Active
                                                        </button>
                                                    @else
                                                        <button type="submit" name="status" value="0"
                                                            class="dropdown-item">
                                                            <i class="bx bx-power-off"></i>&nbsp Disable
                                                        </button>
                                                    @endif
                                                </form>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                                <form method="post" action="{{ route('admin-kategori-delete', ['id' => $row->id]) }}"
                                    id="delete-{{ $row->id }}">
                                    @csrf
                                    @method('delete')
                                </form>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"
        integrity="sha256-oP6HI9z1XaZNBrJURtCoUT5SUnxFr8s3BzRl+cbzUq8=" crossorigin="anonymous"></script>
    <link href="https://cdn.datatables.net/v/bs5/dt-1.13.4/datatables.min.css" rel="stylesheet" />
    <script src="https://cdn.datatables.net/v/bs5/dt-1.13.4/datatables.min.js"></script>
    <script>
        let table = $('#kategoriTable').DataTable({
            searching: true,
            pagination: true
        });
        $('#kategoriTable_filter input[type="search"]').attr('placeholder', 'Search All Fields').addClass(
            'form-control');
        $('#searchBox').on('keyup', function() {
            table.search(this.value).draw();
        });
    </script>
    <div class="modal fade" id="addModal" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalCenterTitle">Tambah Kategori Blog</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('admin-kategori-store') }}" method="post">
                        @csrf
                        <div class="col mb-3">
                            <label for="nameWithTitle" class="form-label">Nama Kategori</label><span
                                class="text-danger">*</span>
                            <input type="text" name="nama_kategori" id="nameWithTitle"
                                class="form-control @error('nama_kategori') is-invalid @enderror"
                                value="{{ old('nama_kategori') }}" placeholder="Nama Kategori" />
                            @error('nama_kategori')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-bs-dismiss="modal">
                        Close
                    </button>
                    <button type="submit" class="btn btn-primary"> Save </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endSection

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>My Blog</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="{{ 'blog_template' }}/assets/img/favicon.png" rel="icon">
    <link href="{{ 'blog_template' }}/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=EB+Garamond:wght@400;500&family=Inter:wght@400;500&family=Playfair+Display:ital,wght@0,400;0,700;1,400;1,700&display=swap"
        rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{ 'blog_template' }}/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ 'blog_template' }}/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="{{ 'blog_template' }}/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
    <link href="{{ 'blog_template' }}/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="{{ 'blog_template' }}/assets/vendor/aos/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('admin_template') }}/assets/vendor/fonts/boxicons.css" />
    <!-- Template Main CSS Files -->
    <link href="{{ 'blog_template' }}/assets/css/variables.css" rel="stylesheet">
    <link href="{{ 'blog_template' }}/assets/css/main.css" rel="stylesheet">

    <!-- =======================================================
  * Template Name: ZenBlog
  * Updated: Jul 27 2023 with Bootstrap v5.3.1
  * Template URL: https://bootstrapmade.com/zenblog-bootstrap-blog-template/
  * Author: BootstrapMade.com
  * License: https:///bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

    <!-- ======= Header ======= -->
    <header id="header" class="header d-flex align-items-center fixed-top">
        <div class="container-fluid container-xl d-flex align-items-center justify-content-between">

            <a href="{{ route('landing-manage') }}" class="logo d-flex align-items-center">
                <!-- Uncomment the line below if you also wish to use an image logo -->
                <!-- <img src="{{ 'blog_template' }}/assets/img/logo.png" alt=""> -->
                <h1>MyBlog</h1>
            </a>

            <nav id="navbar" class="navbar">
                <ul>
                    <li><a href="{{ route('landing-manage') }}">Blog</a></li>
                </ul>
            </nav><!-- .navbar -->

            <div class="position-relative">
                <a href="#" class="mx-2"><span class="bi-facebook"></span></a>
                <a href="#" class="mx-2"><span class="bi-twitter"></span></a>
                <a href="#" class="mx-2"><span class="bi-instagram"></span></a>

                <a href="#" class="mx-2 js-search-open"><span class="bi-search"></span></a>
                <i class="bi bi-list mobile-nav-toggle"></i>

                <!-- ======= Search Form ======= -->
                <div class="search-form-wrap js-search-form-wrap">
                    <form action="{{ route('landing-manage') }}" class="search-form" method="GET">
                        <span class="icon bi-search"></span>
                        <input type="text" name="query" placeholder="Search" value="{{ request('query') }}"
                            class="form-control">
                        <button class="btn js-search-close" type="button"><span class="bi-x"></span></button>
                    </form>
                </div>
            </div>
        </div>
    </header><!-- End Header -->
    <main id="main">
        <!-- ======= Hero Slider Section ======= -->
        <section id="hero-slider" class="hero-slider">
            <div class="container-md" data-aos="fade-in">
                <div class="row">
                    <div class="col-12">
                        <div class="swiper sliderFeaturedPosts">
                            <div class="swiper-wrapper">

                                @foreach ($carouselPosts as $posted)
                                    <div class="swiper-slide">
                                        <a href="{{ route('landing-details', ['id' => $posted->id]) }}"
                                            class="img-bg d-flex align-items-end"
                                            style="background-image: url('{{ asset('image_blog/thumbnail/' . $posted->thumbnail) }}');">
                                            <div class="img-bg-inner">
                                                <h2>{{ $posted->judul }}</h2>
                                            </div>
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                            <div class="custom-swiper-button-next">
                                <span class="bi-chevron-right"></span>
                            </div>
                            <div class="custom-swiper-button-prev">
                                <span class="bi-chevron-left"></span>
                            </div>

                            <div class="swiper-pagination"></div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- End Hero Slider Section -->

        <!-- ======= Post Grid Section ======= -->
        <section id="posts" class="posts">
            <div class="container" data-aos="fade-up">
                <div class="row">
                    <div class="col-md-9">
                        <div class="row">
                            <form id="filter-form" action="{{ route('landing-manage') }}" method="get">
                                <div class="col-md-3 mb-3">
                                    <div class="input-group">
                                        <span class="input-group-text"><i class="bx bx-filter-alt"></i></span>
                                        <select name="kategori" class="form-control" onchange="submitForm()">
                                            <option value="">Filter...</option>
                                            @foreach ($kategori as $item)
                                                <option value="{{ $item->id }}"
                                                    @if (request()->get('kategori') == $item->id) selected @endif>
                                                    {{ $item->nama_kategori }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <a href="{{ route('landing-manage') }}" class="btn btn-secondary">Reset</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="row">
                            @forelse ($post as $item)
                                <div class="col-md-4">
                                    <div class="post-entry-1 lg">
                                        <a href="{{ route('landing-details', ['id' => $item->id]) }}"><img
                                                src="{{ asset('image_blog/thumbnail/' . $item->thumbnail) }}"
                                                alt="" class="img-fluid"></a>
                                        <div class="post-meta"><span
                                                class="date">{{ $item->kategori->nama_kategori }}</span>
                                            <span class="mx-1">&bullet;</span>
                                            <span>{{ \Carbon\Carbon::parse($item->tanggal)->format('d F Y') }}</span>
                                        </div>
                                        <h2><a
                                                href="{{ route('landing-details', ['id' => $item->id]) }}">{{ $item->judul }}</a>
                                        </h2>
                                        <p class="mb-4 d-block">{{ Str::limit($item->deskripsi, 120, '...') }}</p>
                                        {{-- <div class="d-flex align-items-center author">
                                            <div class="photo"><img src="assets/img/person-1.jpg" alt=""
                                                    class="img-fluid"></div>
                                            <div class="name">
                                                <h3 class="m-0 p-0">Cameron Williamson</h3>
                                            </div>
                                        </div> --}}

                                    </div>
                                </div>
                            @empty
                                <center>
                                    <h4 class="mt-5">No data available.</h4>
                                </center>
                            @endforelse
                        </div>
                        <div class="text-start py-4">
                            <div class="text-start py-4">
                                {{ $post->links() }}
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-3">
                        <div class="trending">
                            <h3>Newest</h3>
                            <ul class="trending-post">
                                @foreach ($newest as $item)
                                    <li>
                                        <a href="{{ route('landing-details', ['id' => $item->id]) }}">
                                            <span class="number">{{ $loop->iteration }}</span>
                                            <h3>{{ $item->judul }}</h3>
                                            <span
                                                class="author">{{ \Carbon\Carbon::parse($item->tanggal)->format('d F Y') }}</span>
                                        </a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </section>

    </main><!-- End #main -->
    <footer id="footer" class="footer">

        <div class="footer-content">
            <div class="container">

                <div class="row g-5">
                    <div class="col-lg-4">
                        <h3 class="footer-heading">About MyBlog</h3>
                        <p>
                            MyBlog is a dynamic and versatile blogging platform designed to empower users to share their
                            thoughts, ideas, and experiences with the world. With a user-friendly interface and a range
                            of customizable features, MyBlog offers a seamless experience for both bloggers and readers
                            alike.</p>
                        <p><a href="#" class="footer-link-more">Learn More</a></p>
                    </div>
                    <div class="col-6 col-lg-2">

                    </div>
                    <div class="col-6 col-lg-2">
                        <h3 class="footer-heading">Categories</h3>
                        <ul class="footer-links list-unstyled">
                            @foreach ($kategori as $item)
                                <li><a href="{{ route('landing-manage', ['kategori' => $item->id]) }}"><i
                                            class="bi bi-chevron-right"></i>
                                        {{ $item->nama_kategori }}</a></li>
                            @endforeach
                        </ul>
                    </div>

                    <div class="col-lg-4">
                        <h3 class="footer-heading">Recent Posts</h3>

                        <ul class="footer-links footer-blog-entry list-unstyled">
                            @foreach ($recent as $item)
                                <li>
                                    <a href="{{ route('landing-details', ['id' => $item->id]) }}"
                                        class="d-flex align-items-center">
                                        <img src="{{ 'image_blog/thumbnail/' . $item->thumbnail }}" alt=""
                                            class="img-fluid me-3">
                                        <div>
                                            <div class="post-meta d-block"><span
                                                    class="date">{{ $item->kategori->nama_kategori }}</span> <span
                                                    class="mx-1">&bullet;</span>
                                                <span>{{ \Carbon\Carbon::parse($item->tanggal)->format('d F Y') }}</span>
                                            </div>
                                            <span>{{ Str::limit($item->deskripsi, 50, '...') }}</span>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer-legal">
            <div class="container">

                <div class="row justify-content-between">
                    <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                        <div class="copyright">
                            © Copyright <strong><span>MyBlog</span></strong>. All Rights Reserved
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </footer>

    <a href="#" class="scroll-top d-flex align-items-center justify-content-center"><i
            class="bi bi-arrow-up-short"></i></a>

    <!-- Vendor JS Files -->
    <script src="{{ 'blog_template' }}/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="{{ 'blog_template' }}/assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="{{ 'blog_template' }}/assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="{{ 'blog_template' }}/assets/vendor/aos/aos.js"></script>
    <script src="{{ 'blog_template' }}/assets/vendor/php-email-form/validate.js"></script>

    <!-- Template Main JS File -->
    <script src="{{ 'blog_template' }}/assets/js/main.js"></script>

</body>

</html>


<script>
    function submitForm() {
        document.getElementById('filter-form').submit();
    }
</script>

<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\LandingController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\AuthController;

Route::group(['prefix' => '/blog', 'middleware' => 'auth'], function () {
    Route::get("/", [PostController::class, "index"])->name('admin-blog-manage');
    Route::post("/store", [PostController::class, "store"])->name('admin-blog-store');
    Route::put("/update/{id}", [PostController::class, "update"])->name('admin-blog-update');
    Route::get("/{id}", [PostController::class, "details"])->name('admin-blog-details');
    Route::delete("/delete/{id}", [PostController::class, "delete"])->name('admin-blog-delete');
});

Route::group(['prefix' => '/login'], function () {
    Route::get("/", [AuthController::class, "login"])->name('admin-login');
    Route::post("/post", [AuthController::class, "submitLogin"])->name('admin-submitLogin');
    Route::get("/logout", [AuthController::class, "logout"])->name('admin-logout');
});

Route::group(['prefix' => '/dashboard', 'middleware' => 'auth'], function () {
    Route::get("/", [DashboardController::class, "index"])->name('admin-dashboard');
    Route::get("/get-chartData", [DashboardController::class, "chartData"])->name('admin-dashboard-chart-data');
});

Route::group(['prefix' => '/kategori', 'middleware' => 'auth'], function () {
    Route::get("/", [KategoriController::class, "index"])->name('admin-kategori-manage');
    Route::post("/store", [KategoriController::class, "store"])->name('admin-kategori-store');
    Route::put("/update/{id}", [KategoriController::class, "update"])->name('admin-kategori-update');
    Route::put("/update/status/{id}", [KategoriController::class, "updateStatus"])->name('admin-kategori-updateStatus');
    Route::delete("/delete/{id}", [KategoriController::class, "delete"])->name('admin-kategori-delete');
});

Route::group(['prefix' => '/'], function () {
    Route::get("/", [LandingController::class, "index"])->name('landing-manage');
    Route::get("/{id}", [LandingController::class, "details"])->name('landing-details');
});




// Route::group(['prefix' => '/login'], function () {
//     Route::get("/", [LoginController::class, "index"])->name('landing-manage');
//     Route::post("/store", [LoginController::class, "store"])->name('landing-store');
//     Route::put("/update/{id}", [LoginController::class, "update"])->name('landing-update');
//     Route::delete("/delete/{id}", [LoginController::class, "delete"])->name('landing-delete');
// });



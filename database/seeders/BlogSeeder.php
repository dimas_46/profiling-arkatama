<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Ambil semua kategori_id dari tabel kategori
        $kategoriIds = DB::table('kategoris')->pluck('id');

        // Generate data untuk seeding
        $jumlahData = 10; // Ubah sesuai dengan jumlah data yang diinginkan
        $data = [];
        $now = Carbon::now();

        for ($i = 0; $i < $jumlahData; $i++) {
            $data[] = [
                'judul' => "Judul Post $i",
                'thumbnail' => "thumbnail_$i.jpg",
                'deskripsi' => "Deskripsi post $i",
                'tanggal' => $now,
                'kategori_id' => $kategoriIds->random(),
                'created_at' => $now,
                'updated_at' => $now,
            ];
        }

        // Lakukan proses seeding
        DB::table('posts')->insert($data);
    }
}

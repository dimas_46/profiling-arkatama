<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Kategori;
use Carbon\Carbon;
use Validator;
use App\Models\PostImage;

class PostController extends Controller
{
    public function index()
    {
        $post = Post::with([
            'images',
            'kategori'
        ])->get();
        $kategori = Kategori::where('status', 1)->get();
        $data = [
            'post' => $post,
            'kategori' => $kategori
        ];
        return view('admin.post.manage', $data);
    }

    public function store(Request $request)
    {
        $rules = [
            'judul' => 'required|unique:posts,judul',
            'thumbnail' => 'required|mimes:jpg,jpeg,png|max:2048',
            'deskripsi' => 'required',
            'kategori_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $post = new Post();
        $post->judul = $request->judul;
        if ($request->hasFile('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $thumbnailName = uniqid() . '.' . $thumbnail->getClientOriginalExtension();
            $thumbnail->move(public_path('image_blog/thumbnail'), $thumbnailName);
            $post->thumbnail = $thumbnailName;
        }
        $post->deskripsi = $request->deskripsi;
        $post->tanggal = Carbon::now();
        $post->kategori_id = $request->kategori_id;
        $post->save();

        if ($request->hasFile('image')) {
            $images = $request->file('image');
            foreach ($images as $image) {
                $imageName = uniqid() . '.' . $image->getClientOriginalName();
                $image->move(public_path('image_blog/image'), $imageName);

                $postImage = new PostImage;
                $postImage->post_id = $post->id;
                $postImage->image = $imageName;
                $postImage->save();
            }
        }

        return redirect()->back()->with('success', 'Data berita berhasil ditambah!');
    }

    public function delete($id)
{
    $post = Post::findOrFail($id);

    if ($post) {
        if ($post->thumbnail != null) {
            $oldImage = public_path('image_blog/thumbnail/' . $post->thumbnail);
            if (file_exists($oldImage)) {
                unlink($oldImage);
            }
        }
        $postImages = PostImage::where('post_id', $post->id)->get();
        foreach ($postImages as $postImage) {
            $imagePath = public_path('image_blog/image/' . $postImage->image);
            if (file_exists($imagePath)) {
                unlink($imagePath);
            }
            $postImage->delete();
        }
        $post->delete();

        return redirect()->back()->with('success', 'Data berita berhasil dihapus!');
    }

    return redirect()->back()->withErrors('Data berita tidak ditemukan!');
}

    public function details($id)
    {
        $post = Post::with([
            'kategori',
            'images'
        ])->findOrFail($id);

        $data = [
            'post' => $post
        ];

        return view('admin.post.details', $data);
    }
    public function update(Request $request, $id)
    {
        $rules = [
            'judul' => 'required|unique:posts,judul,' . $id,
            'thumbnail' => 'nullable|mimes:jpg,jpeg,png',
            'deskripsi' => 'required',
            'kategori_id' => 'required'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $post = Post::findOrFail($id);
        $post->judul = $request->judul;
        $post->deskripsi = $request->deskripsi;
        $post->kategori_id = $request->kategori_id;

        if ($request->hasFile('thumbnail')) {
            $thumbnail = $request->file('thumbnail');
            $thumbnailName = uniqid() . '.' . $thumbnail->getClientOriginalExtension();
            $thumbnail->move(public_path('image_blog/thumbnail'), $thumbnailName);
            $post->thumbnail = $thumbnailName;
        }

        $post->save();

        if ($request->hasFile('image')) {
            $oldImages = $post->images;
            foreach ($oldImages as $oldImage) {
                $oldImagePath = public_path('image_blog/image/' . $oldImage->image);
                if (file_exists($oldImagePath)) {
                    unlink($oldImagePath);
                }
                $oldImage->delete();
            }

            $images = $request->file('image');
            foreach ($images as $image) {
                $imageName = uniqid() . '.' . $image->getClientOriginalName();
                $image->move(public_path('image_blog/image'), $imageName);

                $postImage = new PostImage;
                $postImage->post_id = $post->id;
                $postImage->image = $imageName;
                $postImage->save();
            }
        }

        return redirect()->back()->with('success', 'Data berita berhasil diperbarui!');
    }

}

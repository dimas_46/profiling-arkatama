<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;
use Validator;

class KategoriController extends Controller
{
    public function index()
    {
        $kategori = Kategori::get();

        $data = [
            'kategori' => $kategori
        ];
        return view('admin.kategori.manage', $data);
    }
    public function store(Request $request)
    {
        $rules = [
            'nama_kategori' => 'required|unique:kategoris,nama_kategori'
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $kategori = new Kategori();
        $kategori->nama_kategori = $request->nama_kategori;
        $kategori->status = true;
        $kategori->save();
        return redirect()->back()->with('success', 'Data kategori berhasil ditambah!');
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'nama_kategori' => 'required|unique:kategoris,nama_kategori,' . $id
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $kategori = Kategori::findOrFail($id);
        $kategori->nama_kategori = $request->nama_kategori;
        $kategori->save();

        return redirect()->back()->with('success', 'Data kategori berhasil diperbaharui!');
    }


    public function updateStatus(Request $request, $id)
    {

        $kategori = Kategori::find($id);

        if ($kategori->status == "1") {
            $kategori->status = "0" ;
            $kategori->save();
        } else {
            $kategori->status = "1" ;
            $kategori->save();
        }
        return redirect()->back()->with('success', 'Status kategori berhasil diupdate!');
    }

    public function delete($id)
    {
        $kategori = Kategori::findOrFail($id);
        $kategori->delete();
        return redirect()->back()->with('success', 'Data kategori berhasil dihapus!');
    }
}

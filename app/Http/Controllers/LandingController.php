<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Kategori;

class LandingController extends Controller
{
    public function index(Request $request)
{
    $query = $request->input('query');
    $kategori = Kategori::where('status', 1)->limit(8)->get();
    $recent = Post::with('kategori')->orderBy('created_at', 'desc')->limit(3)->get();
    $newest = Post::with('kategori')->orderBy('created_at', 'desc')->limit(8)->get();
    $carouselPosts = Post::with([
        'kategori',
        'images'
    ])->orderBy('created_at', 'desc')->limit(4)->get();

    $posts = Post::with('kategori')
        ->where('judul', 'like', '%' . $query . '%')
        ->orWhere('deskripsi', 'like', '%' . $query . '%')
        ->orderBy('created_at', 'desc')
        ->paginate(6);

    $data = [
        'post' => $posts,
        'kategori' => $kategori,
        'recent' => $recent,
        'newest' => $newest,
        'carouselPosts' => $carouselPosts,
        'searchQuery' => $query, // Untuk menampilkan query pencarian di halaman
    ];

    return view('landing.landing', $data);
}


    public function details($id)
    {
        $post = Post::with([
            'kategori',
            'images'
        ])->findOrFail($id);

        $hasImages = $post->images->isNotEmpty(); // Check if there are images connected to the post

        $kategori = Kategori::where('status', 1)->limit(8)->get();
        $recent = Post::with('kategori')->orderBy('created_at', 'desc')->limit(3)->get();
        $latest = Post::with('kategori')->orderBy('created_at', 'desc')->limit(5)->get();

        $data = [
            'post' => $post,
            'kategori' => $kategori,
            'recent' => $recent,
            'latest' => $latest,
            'hasImages' => $hasImages, // Add this line to the data array
        ];

        return view('landing.details', $data);
    }

}

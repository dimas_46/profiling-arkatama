<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kategori;
use App\Models\Post;

class DashboardController extends Controller
{
    public function index()
    {
        $post = Post::count();
        $kategori = Kategori::count();

        $data = [
            'post' => $post,
            'kategori' => $kategori
        ];
        return view('admin.dashboard', $data);
    }

    public function chartData()
    {
        $kategoriData = Kategori::where('status', 1)->withCount('posts')->get();
        $data = [];

        foreach ($kategoriData as $kategori) {
            $data[] = [
                'nama_kategori' => $kategori->nama_kategori,
                'post_count' => $kategori->posts_count,
            ];
        }

        return response()->json($data);
    }
}
